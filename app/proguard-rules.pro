-keepattributes SourceFile,LineNumberTable
-dontwarn retrofit2.**
-dontwarn okio.**
-dontwarn javax.annotation.**
-keep class retrofit2.** { *; }
-keepattributes Signature
-keepattributes Exceptions
-keepclasseswithmembers class * {
    @retrofit2.http.* <methods>;
}

#BUTTERKNIFE
# Retain generated class which implement Unbinder.
-keep public class * implements butterknife.Unbinder { public <init>(**, android.view.View); }
# Prevent obfuscation of types which use ButterKnife annotations since the simple name
# is used to reflectively look up the generated ViewBinding.
-keep class butterknife.*
-keepclasseswithmembernames class * { @butterknife.* <methods>; }
-keepclasseswithmembernames class * { @butterknife.* <fields>; }

# RXJAVA RXANDROID
-keep class rx.schedulers.Schedulers {public static <methods>;}
-keep class rx.schedulers.ImmediateScheduler {public <methods>;}
-keep class rx.schedulers.TestScheduler {public <methods>;}
-keep class rx.schedulers.Schedulers {public static ** test();}

# MOXY COMPILER
-dontwarn com.arellomobile.mvp.MoxyReflector

