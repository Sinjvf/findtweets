package ru.examples.sinjvf.twitter.presentation.tweets.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.tweetui.CompactTweetView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.examples.sinjvf.twitter.R;
import timber.log.Timber;

/**
 * Created by Sinjvf on 04.11.2017.
 * Holder for recycler adapter with tweets
 */

class TweetHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tweetLayout)
    FrameLayout tweetLayout;

     TweetHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
    void bindTweet(Tweet tweet){
        tweetLayout.removeAllViews();
        CompactTweetView tweetView = new CompactTweetView(itemView.getContext(), tweet);
        tweetLayout.addView(tweetView);
        Timber.d("bindTweet: "+tweet.id);
    }

    void bindLoader(){
        tweetLayout.removeAllViews();
        ProgressBar progress = new ProgressBar(itemView.getContext());
        tweetLayout.addView(progress);
    }
}
