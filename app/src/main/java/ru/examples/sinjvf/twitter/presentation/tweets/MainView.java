package ru.examples.sinjvf.twitter.presentation.tweets;

import com.arellomobile.mvp.MvpView;
import com.twitter.sdk.android.core.models.Tweet;

import java.util.List;

/**
 * Created by Sinjvf on 04.11.2017.
 * MVP view for main activity
 */

public interface MainView extends MvpView {
    void updateList(List<Tweet> list);
    void addItems(List<Tweet> list);
    void stopLoad();
}
