package ru.examples.sinjvf.twitter.data.network;

import com.twitter.sdk.android.core.models.Search;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * Created by Sinjvf on 04.11.2017.
 * implementation of NetworkRepository interface
 */

public class NetworkRepositoryImpl implements NetworkRepository{
    private MySearchService service;

    @Inject
    public NetworkRepositoryImpl(MySearchService service) {
        this.service = service;
    }

    public Single<Search> getTweets(String query,
                                    Integer count,
                                    String until,
                                    Long sinceId,
                                    Long maxId,
                                    String resultType){
        return service.search(query, count, until, sinceId, maxId, resultType);
    }
}
