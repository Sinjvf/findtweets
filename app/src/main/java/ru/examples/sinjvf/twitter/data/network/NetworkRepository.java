package ru.examples.sinjvf.twitter.data.network;

import com.twitter.sdk.android.core.models.Search;

import io.reactivex.Single;

/**
 * Created by Sinjvf on 04.11.2017.
 * interface for tweets searching
 */

public interface NetworkRepository {
    Single<Search> getTweets(String query,
                             Integer count,
                             String until,
                             Long sinceId,
                             Long maxId,
                             String resultType);
}
