package ru.examples.sinjvf.twitter.presentation.tweets;

import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.jakewharton.rxbinding2.support.v7.widget.RxRecyclerView;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.twitter.sdk.android.core.models.Tweet;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.examples.sinjvf.twitter.R;
import ru.examples.sinjvf.twitter.presentation.TweetsApplication;
import ru.examples.sinjvf.twitter.presentation.tweets.adapter.TweetsAdapter;

public class MainActivity extends MvpAppCompatActivity implements MainView {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.inputText)
    TextInputEditText inputText;
    private TweetsAdapter adapter = new TweetsAdapter();

    @InjectPresenter
    MainPresenter presenter;

    @ProvidePresenter
    MainPresenter providePresenter() {
        return TweetsApplication.getAppComponent().provideMainPresenter();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initToolbar();
        initRecycler();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.setTextObservable(RxTextView.afterTextChangeEvents(inputText));
        presenter.setRecyclerObservable(RxRecyclerView.scrollEvents(recyclerView),
                (LinearLayoutManager) recyclerView.getLayoutManager(),
                adapter);
    }

    private void initToolbar(){
        setSupportActionBar(toolbar);
        ActionBar bar = getSupportActionBar();
        if (bar!=null)
            bar.setDisplayShowTitleEnabled(true);
    }

    private void initRecycler() {
        LinearLayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(false);
    }

    @Override
    public void updateList(List<Tweet> list) {
        adapter.updateList(list);
    }

    @Override
    public void addItems(List<Tweet> list) {
        adapter.addItems(list);
    }

    @Override
    public void stopLoad() {
        adapter.stopLoad();
    }

}
