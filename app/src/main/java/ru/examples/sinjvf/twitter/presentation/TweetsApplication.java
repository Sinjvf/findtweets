package ru.examples.sinjvf.twitter.presentation;

import android.app.Application;
import android.util.Log;

import com.squareup.leakcanary.LeakCanary;
import com.twitter.sdk.android.core.Twitter;

import ru.examples.sinjvf.twitter.BuildConfig;
import ru.examples.sinjvf.twitter.di.AppComponent;
import ru.examples.sinjvf.twitter.di.DaggerAppComponent;
import timber.log.Timber;

/**
 * Created by Sinjvf on 04.11.2017.
 * App class
 */

public class TweetsApplication extends Application {

    private static AppComponent appComponent;
    @Override
    public void onCreate() {
        super.onCreate();

        Twitter.initialize(this);

        appComponent = buildComponent();
        if (!BuildConfig.release) {
            Timber.plant(new Timber.DebugTree());
        } else {
            Timber.plant(new CrashReportingTree());
        }

        LeakCanary.install(this);
    }

    private static class CrashReportingTree extends Timber.Tree {
        @Override
        protected void log(int priority, String tag, String message, Throwable t) {
            if (priority == Log.VERBOSE || priority == Log.DEBUG) {
                return;
            }
            log(priority, tag, message);
        }
    }

    private AppComponent buildComponent() {
        return DaggerAppComponent.builder()
                .build();
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }
}
