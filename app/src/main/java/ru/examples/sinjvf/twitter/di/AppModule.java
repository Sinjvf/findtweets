package ru.examples.sinjvf.twitter.di;


import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.examples.sinjvf.twitter.business.RxSchedulers;
import ru.examples.sinjvf.twitter.business.SearchInteractor;
import ru.examples.sinjvf.twitter.data.network.MySearchService;
import ru.examples.sinjvf.twitter.data.network.MyTwitterApiClient;
import ru.examples.sinjvf.twitter.data.network.NetworkRepository;
import ru.examples.sinjvf.twitter.data.network.NetworkRepositoryImpl;
import ru.examples.sinjvf.twitter.presentation.tweets.MainPresenter;

/**
 * Created by Sinjvf on 04.11.2017.
 * dagger's module. Provides Twitter  Network SearchService
 */
@Module
public class AppModule {
    @Provides
    @Singleton
    MySearchService provideApi() {
        MyTwitterApiClient twitterApiClient = new MyTwitterApiClient();
        return twitterApiClient.getMySeacrhService();
    }

    @Singleton
    @Provides
    NetworkRepository provideRepository(MySearchService service) {
        return new NetworkRepositoryImpl(service);
    }

    @Singleton
    @Provides
    SearchInteractor provideInteractor(RxSchedulers schedulers, NetworkRepository repository) {
        return new SearchInteractor(schedulers, repository);
    }

    @Provides
    MainPresenter provideMainPresenter(SearchInteractor interactor) {
        return new MainPresenter(interactor);
    }

    @Singleton
    @Provides
    RxSchedulers provideSchedulers() {
        return new RxSchedulers();
    }
}
