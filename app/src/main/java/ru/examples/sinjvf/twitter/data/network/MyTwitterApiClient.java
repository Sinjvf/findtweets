package ru.examples.sinjvf.twitter.data.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.internal.TwitterApi;
import com.twitter.sdk.android.core.internal.network.OkHttpClientHelper;
import com.twitter.sdk.android.core.models.BindingValues;
import com.twitter.sdk.android.core.models.BindingValuesAdapter;
import com.twitter.sdk.android.core.models.SafeListAdapter;
import com.twitter.sdk.android.core.models.SafeMapAdapter;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Sinjvf on 04.11.2017.
 * API client with Service witch works with Rx
 */

public class MyTwitterApiClient extends TwitterApiClient {
    public MyTwitterApiClient() {
        super();
    }

    private Retrofit buildRetrofit(OkHttpClient httpClient, TwitterApi twitterApi) {
        return new Retrofit.Builder()
                .client(httpClient)
                .baseUrl(twitterApi.getBaseHostUrl())
                .addConverterFactory(GsonConverterFactory.create(buildGson()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    private Gson buildGson() {
        return new GsonBuilder()
                .registerTypeAdapterFactory(new SafeListAdapter())
                .registerTypeAdapterFactory(new SafeMapAdapter())
                .registerTypeAdapter(BindingValues.class, new BindingValuesAdapter())
                .create();
    }

    public MySearchService getMySeacrhService() {
       return buildRetrofit(OkHttpClientHelper.getOkHttpClient(
               TwitterCore.getInstance().getGuestSessionProvider()),
               new TwitterApi()).create(MySearchService.class);
    }
}
