package ru.examples.sinjvf.twitter.data.network;

import com.twitter.sdk.android.core.models.Search;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Sinjvf on 04.11.2017.
 * Search Service. The same as  Twitter's SearchService, but works with Rx
 */

public interface MySearchService {
    /**
     * Returns a collection of relevant Tweets matching a specified query.
     * <p>
     * Please note that Twitter's search service and, by extension, the Search API is not meant to
     * be an exhaustive source of Tweets. Not all Tweets will be indexed or made available via the
     * search interface.
     * <p>
     * In API v1.1, the response format of the Search API has been improved to return Tweet objects
     * more similar to the objects you'll find across the REST API and platform. You may need to
     * tolerate some inconsistencies and variance in perspectival values (fields that pertain to the
     * perspective of the authenticating user) and embedded user objects.
     * <p>
     * To learn how to use Twitter Search effectively, consult our guide to Using the Twitter Search
     * API. See Working with Timelines to learn best practices for navigating results by since_id
     * and max_id.
     *
     * @param query (required) A UTF-8, URL-encoded search query of 500 characters maximum,
     *              including operators. Queries may additionally be limited by complexity.
     * @param count (optional) The number of tweets to return per page, up to a maximum of 100.
     *              Defaults to 15. This was formerly the "rpp" parameter in the old Search API.
     * @param until (optional) Returns tweets generated before the given date. Date should be
     *              formatted as YYYY-MM-DD. Keep in mind that the search index may not go back as
     *              far as the date you specify here.
     * @param sinceId (optional) Returns results with an ID greater than (that is, more recent than)
     *                the specified ID. There are limits to the number of Tweets which can be
     *                accessed through the API. If the limit of Tweets has occured since the
     *                since_id, the since_id will be forced to the oldest ID available.
     * @param maxId (optional) Returns results with an ID less than (that is, older than) or equal
     *              to the specified ID.
     * @param resultType (optional) Specifies what type of search results you would prefer to
     *                   receive. The current default is "mixed." Valid values include:
     * mixed: Include both popular and real time results in the response.
     * recent: return only the most recent results in the response
     * popular: return only the most popular results in the response.
     */
    @GET("/1.1/search/tweets.json?" +
            "tweet_mode=extended&include_cards=true&cards_platform=TwitterKit-13")
    Single<Search> search(@Query("q") String query,
                          @Query("count") Integer count,
                          @Query("until") String until,
                          @Query("since_id") Long sinceId,
                          @Query("max_id") Long maxId,
                          @Query("result_type") String resultType);
}
