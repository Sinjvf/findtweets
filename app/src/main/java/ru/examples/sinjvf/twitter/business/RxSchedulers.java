package ru.examples.sinjvf.twitter.business;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Sinjvf on 04.11.2017.
 * schedulers for rx subscribeOn and observeOn
 */

public class RxSchedulers{

    public Scheduler getUiScheduler() {
        return AndroidSchedulers.mainThread();
    }

    public Scheduler getIoScheduler() {
        return Schedulers.io();
    }

}
