package ru.examples.sinjvf.twitter.presentation.tweets.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.twitter.sdk.android.core.models.Tweet;

import java.util.ArrayList;
import java.util.List;

import ru.examples.sinjvf.twitter.R;

/**
 * Created by Sinjvf on 04.11.2017.
 * adapter for tweets recycler view
 */

public class TweetsAdapter extends RecyclerView.Adapter<TweetHolder> {
    private static final int LOADER = 1;
    private static final int TWEET = 0;
    private List<Tweet> list;
    private int addLoading = 0;

    public void updateList(List<Tweet> list) {
        this.list = new ArrayList<>(list);
        addLoading = 1;
        notifyDataSetChanged();
    }

    public void addItems(List<Tweet> list) {
        int oldCount = getItemCount();
        if (this.list != null) {
            this.list.addAll(list);
            notifyItemRangeInserted(oldCount, getItemCount());
        } else
            updateList(list);
    }

    @Override
    public TweetHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == LOADER) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_tweet, parent, false);
            return new TweetHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_tweet, parent, false);
            return new TweetHolder(view);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (addLoading == 1 && position == getItemCount() - 1)
            return LOADER;
        return TWEET;
    }

    @Override
    public void onBindViewHolder(TweetHolder holder, int position) {
        int listSize = list == null ? 0 : list.size();
        if (position < listSize) {
            assert list != null;
            holder.bindTweet(list.get(position));
        } else
            holder.bindLoader();
    }

    @Override
    public int getItemCount() {
        return list == null ? addLoading : list.size() + addLoading;
    }

    public void stopLoad() {
        addLoading = 0;
        notifyItemRemoved(getItemCount() + 1);
    }
}
