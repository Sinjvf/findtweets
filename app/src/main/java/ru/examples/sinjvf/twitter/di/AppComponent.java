package ru.examples.sinjvf.twitter.di;

import javax.inject.Singleton;

import dagger.Component;
import ru.examples.sinjvf.twitter.presentation.tweets.MainPresenter;

/**
 * Created by Sinjvf on 04.11.2017.
 * Dagger's component
 */
@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {
    MainPresenter provideMainPresenter();
}
