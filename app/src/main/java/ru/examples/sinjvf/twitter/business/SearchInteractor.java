package ru.examples.sinjvf.twitter.business;

import android.support.annotation.Nullable;

import com.twitter.sdk.android.core.models.Tweet;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import ru.examples.sinjvf.twitter.data.network.NetworkRepository;

/**
 * Created by Sinjvf on 04.11.2017.
 * class for transform network data to data for presentation layer
 */

public class SearchInteractor {
    private static final int COUNT = 20;
    private RxSchedulers schedulers;
    private NetworkRepository repository;
    private String currentQuery;
    private Long currentLastId;

    @Inject
    public SearchInteractor(RxSchedulers schedulers, NetworkRepository repository) {
        this.schedulers = schedulers;
        this.repository = repository;
    }

    /**
     * @return next COUNT tweets with old hashTag
     */
    public Single<List<Tweet>> getNextTweets() {
        return repository.getTweets(currentQuery, COUNT, null, null, currentLastId - 1, "recent")
                .subscribeOn(schedulers.getIoScheduler())
                .map(search -> search.tweets)
                .map(this::tweetsPreparing)
                .observeOn(schedulers.getUiScheduler());

    }


    /**
     * set new hashTag and
     *
     * @param query0 - String of hashtag without '#'
     * @return first COUNT tweets
     */
    public Single<List<Tweet>> getTweets(String query0) {
        String query = "#" + query0;
        return repository.getTweets(query, COUNT, null, null, null, "recent")
                .subscribeOn(schedulers.getIoScheduler())
                .map(search -> search.tweets)
                .map(tweets -> {
                    currentQuery = query;
                    return tweetsPreparing(tweets);
                })
                .observeOn(schedulers.getUiScheduler());
    }

    /**
     * save id of the last tweet and
     *
     * @param tweets    - list of tweets
     * @return new List, because previous list was unmodified
     *
     */
    private List<Tweet> tweetsPreparing(@Nullable List<Tweet> tweets)  {
        List<Tweet> newList = new ArrayList<>();
        if (tweets != null && tweets.size() != 0) {
            newList.addAll(tweets);
            currentLastId = tweets.get(tweets.size() - 1).getId();
        }
        return newList;
    }
}
