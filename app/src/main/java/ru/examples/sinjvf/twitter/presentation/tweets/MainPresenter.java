package ru.examples.sinjvf.twitter.presentation.tweets;


import android.support.v7.widget.LinearLayoutManager;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.jakewharton.rxbinding2.support.v7.widget.RecyclerViewScrollEvent;
import com.jakewharton.rxbinding2.widget.TextViewAfterTextChangeEvent;
import com.twitter.sdk.android.core.models.Tweet;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import ru.examples.sinjvf.twitter.business.SearchInteractor;
import ru.examples.sinjvf.twitter.presentation.tweets.adapter.TweetsAdapter;
import timber.log.Timber;

/**
 * Created by Sinjvf on 04.11.2017.
 * Presenter for main screen
 */

@InjectViewState
public class MainPresenter extends MvpPresenter<MainView> {
    private static final int TIMEOUT = 500;
    private static final int MIN_LENGTH = 3;

    private SearchInteractor repository;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private boolean reachBottom;

    @Inject
    public MainPresenter(SearchInteractor repository) {
        this.repository = repository;
    }

    public void setTextObservable(Observable<TextViewAfterTextChangeEvent> observable) {
        compositeDisposable.add(observable.skip(1)
                .debounce(TIMEOUT, TimeUnit.MILLISECONDS)
                .filter(event -> event.view().getText().toString().length() >= MIN_LENGTH)
                .map(event -> event.view().getText().toString())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::textChanged, this::handleError));
    }

    public void setRecyclerObservable(Observable<RecyclerViewScrollEvent> observable,
                                      LinearLayoutManager manager,
                                      TweetsAdapter adapter) {
        compositeDisposable.add(observable.skip(1)
                .debounce(TIMEOUT, TimeUnit.MILLISECONDS)
                .filter(event ->
                        !reachBottom &&
                                manager.findLastVisibleItemPosition() == adapter.getItemCount() - 1)
                .subscribe((event) -> scrollToBottom(), this::handleError));
    }

    private void scrollToBottom() {
        repository.getNextTweets()
                .subscribe(list -> addItems(list, false), this::handleError);
    }

    private void textChanged(String text) {
        reachBottom = false;
        getViewState().updateList(new ArrayList<>());
        repository.getTweets(text)
                .subscribe(list -> addItems(list, true), this::handleError);
    }

    private void addItems(List<Tweet> list, boolean isUpdate) {
        if (list == null || list.size() == 0) {
            getViewState().stopLoad();
            reachBottom = true;
        } else {
            if (isUpdate) {
                getViewState().updateList(list);
            }else{
                getViewState().addItems(list);
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (compositeDisposable != null) compositeDisposable.dispose();
    }

    @Override
    public void detachView(MainView view) {
        super.detachView(view);
        if (compositeDisposable != null) compositeDisposable.dispose();
    }

    @Override
    public void attachView(MainView view) {
        compositeDisposable = new CompositeDisposable();
        reachBottom = false;
        super.attachView(view);
    }

    private void handleError(Throwable throwable) {
        Timber.e("handleError: " + throwable.getMessage(), throwable);
    }
}
